<%-- 
    Document   : Persona
    Created on : 24-jul-2018, 23:25:10
    Author     : Alejandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"> -->    
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Persona</title>
  </head>
  <body>
    <h1>Llego desde un servlets SVPERSONA</h1>
    <%String name = (String)request.getAttribute("nom");%>
    <%String apell = (String)request.getAttribute("apell");%>
    
    <p>El Nombre es <%=name %> </p>
    <p>El Apellido es <%=apell %> </p>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
  </body>
</html>
