<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"> -->    
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <title>Index</title>
    </head>
    <body>

        <div class="container">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="text-center">
                    <h2><strong>PERSONA</strong></h2>
                </div>
                <div class="form-group">
                    <form action="Persona" method="post">
                        <input type="text" name="txtNombre" class="form-control" placeholder="ESCRIBA EL NOMBRE"/>
                        <input type="text" name="txtApellido" class="form-control" placeholder="ESCRIBA EL APELLIDO"/>
                        <input type="submit" name="btnEnviar" class="btn btn-primary btn-block" value="ENVIAR"/>
                </div>
                </form>
            </div>
            <div class="col-2"></div>
        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    </body>
</html>
